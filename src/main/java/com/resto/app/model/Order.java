package com.resto.app.model;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "t_order")
public class Order {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
    @Column(name = "customer_id")
    private Long customerId;
    @Column(name = "menu_item_id")
    private Long menuItemId;
    @Column(name = "qty", length = 5)
    private Integer qty;
    @Column(name = "order_date")
    private Date orderDate;
    @Column(name = "amount")
    private Long amount;

    @Column(name = "created_by", nullable = false)
	private Long createdBy;
	@Column(name = "created_on", nullable = false)
	private Date createdOn;
	@Column(name = "modified_by")
	private Long modifiedBy;
	@Column(name = "modified_on")
	private Date modifiedOn;
	@Column(name = "deleted_by")
	private Long deletedBy;
	@Column(name = "deleted_on")
	private Date deletedOn;
	@Column(name="is_delete", nullable = false)
	private Boolean isDelete = false;

	// JOIN CUSTOMER
	@ManyToOne
	@JoinColumn(name = "customer_id", insertable = false, updatable = false)
	private Customer customer;

    // JOIN MENU ITEM
	@ManyToOne
	@JoinColumn(name = "menu_item_id", insertable = false, updatable = false)
	private MenuItem menuItem;
}
