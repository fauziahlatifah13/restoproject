package com.resto.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestoprojectbeApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestoprojectbeApplication.class, args);
	}

}
