package com.resto.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.resto.app.model.Order;

public interface OrderRepo extends JpaRepository<Order, Long>{
    
}
