package com.resto.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.resto.app.model.Category;
import java.util.List;
import java.util.Optional;


public interface CategoryRepo extends JpaRepository<Category, Long>{

    List<Category> findByIsDelete(Boolean isDelete);

    @Query(value="SELECT * FROM m_category WHERE name = ?1 and is_delete = false", nativeQuery=true)
	Optional<Category> findByNameIsDelete(String name);
	
	@Query(value="SELECT * FROM m_category WHERE name = ?1 and is_delete = true", nativeQuery=true)
	Optional<Category> findByNameDelete(String name);
}
