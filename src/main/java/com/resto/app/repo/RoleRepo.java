package com.resto.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.resto.app.model.Role;
import java.util.List;

public interface RoleRepo extends JpaRepository<Role, Long>{
    
    List<Role> findByIsDelete(Boolean isDelete);
}
