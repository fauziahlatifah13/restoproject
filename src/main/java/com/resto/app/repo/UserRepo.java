package com.resto.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.resto.app.model.Users;
import java.util.List;

public interface UserRepo extends JpaRepository<Users, Long>{
    
    List<Users> findByIsDelete(Boolean isDelete);
}
