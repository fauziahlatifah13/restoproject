package com.resto.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.resto.app.model.MenuItem;
import java.util.List;

public interface MenuItemRepo extends JpaRepository<MenuItem, Long>{

        List<MenuItem> findByIsDelete(Boolean isDelete);
}
