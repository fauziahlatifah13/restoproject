package com.resto.app.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.resto.app.repo.MenuItemRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/menuitem/")
public class MenuItemController {
    
    @Autowired
    private MenuItemRepo menuItemRepo;

        // RESPONSE
	private Map<String, Object> response(String status, String message, Object data) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", status);
		response.put("message", message);
		response.put("data", data);

		return response;
	}
}
