package com.resto.app.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.resto.app.model.Category;
import com.resto.app.repo.CategoryRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/category/")
public class CategoryController {
    
    @Autowired
    private CategoryRepo categoryRepo;

    // RESPONSE
	private Map<String, Object> response(String status, String message, Object data) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", status);
		response.put("message", message);
		response.put("data", data);

		return response;
	}

    // FIND ALL
	@GetMapping("all")
	public ResponseEntity<List<Category>> getAllCategory() {
		try {
			List<Category> listCategory = this.categoryRepo.findByIsDelete(false);
			return new ResponseEntity<List<Category>>(listCategory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<Category>>(HttpStatus.NO_CONTENT);
		}
	}

	// CREATE
	@PostMapping("add")
	public ResponseEntity<Map<String, Object>> saveCategory(@RequestBody Category category) {
		Optional<Category> categoryActive = this.categoryRepo.findByNameIsDelete(category.getName());
		Optional<Category> categoryUnactive = this.categoryRepo.findByNameDelete(category.getName());

		try {
			String strCat = category.getName().trim();
			
			// VALIDASI - nama tidak null atau kosong
			if (category.getName() == null || strCat == "") {
				return new ResponseEntity<>(response("failed", "* Masukkan nama Kategori", new ArrayList<>()),
						HttpStatus.OK);
			// VALIDASI - category sudah tersedia
			} else if (categoryActive.isPresent()) {
				return new ResponseEntity<>(response("failed", "* Kategori sudah ada", new ArrayList<>()),
						HttpStatus.OK);
			// VALIDASI NO DUPLICATE ON DATABASE
			} else if (categoryUnactive.isPresent()) {
				Category categoryNewActive = categoryUnactive.get();
				categoryNewActive.setCreatedBy(1L);
				categoryNewActive.setCreatedOn(new Date());
				categoryNewActive.setIsDelete(false);
				Category catActive = this.categoryRepo.save(categoryNewActive);
				return new ResponseEntity<>(response("success", "Save Data Success", catActive), HttpStatus.CREATED);
			}

			category.setCreatedBy(1L);
			category.setCreatedOn(new Date());
			category.setIsDelete(false);

			Category newCategory = this.categoryRepo.save(category);
			if (newCategory.equals(category)) {
				return new ResponseEntity<>(response("success", "Save Data Success", newCategory),
						HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(response("failed", "Save Failed", newCategory), HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(response("failed", "Save Failed", new ArrayList<>()), HttpStatus.OK);
		}
	}

	// FIND BY ID - untuk update
	@GetMapping("find/{id}")
	public ResponseEntity<Object> getCategoryById(@PathVariable("id") Long id) {
		try {
			Optional<Category> category = this.categoryRepo.findById(id);
			if (category.isPresent()) {
				return new ResponseEntity<>(category, HttpStatus.CREATED);
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	// UPDATE
	@PutMapping("edit/{id}")
	public ResponseEntity<Map<String, Object>> editCategory(
			@PathVariable("id") Long id,
			@RequestBody Category categoryEdit) {
		Optional<Category> categoryActive = this.categoryRepo.findByNameIsDelete(categoryEdit.getName());
		Optional<Category> categoryUnactive = this.categoryRepo.findByNameDelete(categoryEdit.getName());

		try {
			Optional<Category> dataCategory = this.categoryRepo.findById(id);
			String strCat = categoryEdit.getName().trim();

			if (dataCategory.isPresent()) {
				// VALIDASI - nama tidak null atau kosong
				if (categoryEdit.getName() == null || strCat == "") {
					return new ResponseEntity<>(response("failed", "* Masukkan kategori", new ArrayList<>()),
							HttpStatus.OK);
				// VALIDASI - nama sudah tersedia
				} else if (categoryActive.isPresent() && dataCategory.get() != categoryUnactive.get()) {
					return new ResponseEntity<>(
							response("failed", "* Kategori sudah ada", new ArrayList<>()), HttpStatus.OK);
				// VALIDASI NO DUPLICATE ON DATABASE
				} else if (categoryUnactive.isPresent()) {
					Category categoryNonavtive = dataCategory.get();
					categoryNonavtive.setIsDelete(true);
					//Category catNonactive = this.categoryRepo.save(categoryNonavtive);

					Category catNewActive = categoryUnactive.get();
					catNewActive.setModifiedBy(1L);
					catNewActive.setModifiedOn(new Date());
					catNewActive.setIsDelete(false);
					Category catActive = this.categoryRepo.save(catNewActive);
					return new ResponseEntity<>(response("success", "Modified Data Success", catActive),
							HttpStatus.CREATED);
				}

				Category category = dataCategory.get();

				categoryEdit.setId(category.getId());
				categoryEdit.setModifiedBy(1L);
				categoryEdit.setModifiedOn(new Date());
				categoryEdit.setCreatedBy(category.getCreatedBy());
				categoryEdit.setCreatedOn(category.getCreatedOn());
				categoryEdit.setIsDelete(category.getIsDelete());

				Category newCategory = this.categoryRepo.save(categoryEdit);
				if (newCategory.equals(category)) {
					return new ResponseEntity<>(response("success", "Modified Data Success", newCategory),
							HttpStatus.CREATED);
				} else {
					return new ResponseEntity<>(response("failed", "Modified Failed", newCategory), HttpStatus.OK);
				}
			}
		} catch (Exception e) {
			return new ResponseEntity<>(response("failed", "Modified Failed", new ArrayList<>()), HttpStatus.OK);
		}
		return null;
	}

	// DELETE
	@PutMapping("delete/{id}")
	public ResponseEntity<Object> deleteCategory(@PathVariable("id") Long id) {
		try {
			Optional<Category> categoryList = this.categoryRepo.findById(id);

			if (categoryList.isPresent()) {
				Category category = categoryList.get();
				category.setIsDelete(true);
				category.setDeletedBy(1L);
				category.setDeletedOn(new Date());
				this.categoryRepo.save(category);
			}
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
